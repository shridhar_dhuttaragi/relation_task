package com.example.springallrelation.manytomany;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table
public class manytomanyItem {
    @Id
    @GeneratedValue
    int id;

    @Column
    String name;

    @ManyToMany(cascade = CascadeType.ALL,mappedBy = "manytomanyItems")
    private Set<manytomanyCustomer> manytomanyCustomers;

    public Set<manytomanyCustomer> getManytomanyCustomers() {
        return manytomanyCustomers;
    }

    public void setManytomanyCustomers(Set<manytomanyCustomer> manytomanyCustomers) {
        this.manytomanyCustomers = manytomanyCustomers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
