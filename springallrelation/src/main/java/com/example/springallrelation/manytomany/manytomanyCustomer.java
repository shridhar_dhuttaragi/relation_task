package com.example.springallrelation.manytomany;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class manytomanyCustomer {

    @Id
    @GeneratedValue
    int id;

    @Column
    String name;



    @ManyToMany(targetEntity = manytomanyItem.class,cascade = CascadeType.ALL)
//    @JoinColumn(name="fk_itm_id",referencedColumnName = "id")
    @JoinTable(name="manytonmayCustdet",joinColumns = {@JoinColumn(name="cust_id")},
            inverseJoinColumns = {@JoinColumn(name = "FK_itm_id")})
    Set<manytomanyItem> manytomanyItems;

    public Set<manytomanyItem> getManytomanyItems() {
        return manytomanyItems;
    }

    public void setManytomanyItems(Set<manytomanyItem> manytomanyItems) {
        this.manytomanyItems = manytomanyItems;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
