package com.example.springallrelation.manytomany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class manytomanyController {

    @Autowired
    manytomanyRepo manytomanyRepos;

    @RequestMapping("/manytomany")
    public void put(@RequestBody manytomanyCustomer manytomanyCustomers)
    {
        manytomanyRepos.save(manytomanyCustomers);
    }
}
