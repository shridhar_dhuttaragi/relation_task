package com.example.springallrelation.manytoone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class manytooneController {

    @Autowired
    manytooneRepo manytooneRepos;

    @RequestMapping("/manytoone")
    public void add(@RequestBody manytooneItem manytooneItems)
    {
        manytooneRepos.save(manytooneItems);
    }

    @RequestMapping("/getmanytoone")
    public List<?> get()
    {
        return (List<?>) manytooneRepos.findAll();
    }
}
