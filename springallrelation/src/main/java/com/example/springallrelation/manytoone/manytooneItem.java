package com.example.springallrelation.manytoone;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class manytooneItem {
    @Id
    @GeneratedValue
    int id;

    @Column
    String name;

    @ManyToOne(targetEntity = manytooneCustomer.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_itm_id",referencedColumnName = "id")
    manytooneCustomer manytooneCustomers;





    public manytooneCustomer getManytooneCustomers() {
        return manytooneCustomers;
    }

    public void setManytooneCustomers(manytooneCustomer manytooneCustomers) {
        this.manytooneCustomers = manytooneCustomers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
