package com.example.springallrelation.manytoone;

import org.springframework.data.repository.CrudRepository;

public interface manytooneRepo extends CrudRepository<manytooneItem,Integer> {

}
