package com.example.springallrelation.onetomany;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository

public interface EmployeeRepo extends JpaRepository<Employee,Integer> {
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM Address_Table a WHERE a.fk_emp_id = ?1 and a.id = ?2",  nativeQuery = true)
    void deletetheAddress(int fk_emp_id, int id);
}
