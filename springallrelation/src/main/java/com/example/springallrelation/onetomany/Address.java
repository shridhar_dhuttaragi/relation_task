package com.example.springallrelation.onetomany;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;


@Entity
@Table(name="Address_Table")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;


    @Column
    @NotBlank(message = "Address is mandatory")
    String e_address;

    @Column
    @NotBlank(message = "City is mandatory")
    String e_city;

    @Column
    @NotBlank(message = "Country is mandatory")
    String e_country;



    public String getE_address() {
        return e_address;
    }

    public void setE_address(String e_address) {
        this.e_address = e_address;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getE_city() {
        return e_city;
    }

    public void setE_city(String e_city) {
        this.e_city = e_city;
    }

    public String getE_country() {
        return e_country;
    }

    public void setE_country(String e_country) {
        this.e_country = e_country;
    }
}
