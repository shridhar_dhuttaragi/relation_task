package com.example.springallrelation.onetomany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepo employeeRepo;

    public void save(Employee employee) {
        employeeRepo.save(employee);
    }

    public List<Employee> getAll() {
       return employeeRepo.findAll();
    }

    public Employee getEmployeeById(int id) {
        return employeeRepo.findById(id).get();
    }

    public void deleteEmployeeById(int id) {
        employeeRepo.deleteById(id);
    }

    public Employee updateEmployeeById(int id, Employee employee) {

        Employee emp=employeeRepo.findById(id).get();
        emp.setE_name(employee.getE_name());
        emp.setE_designation((employee.getE_designation()));
        emp.setAddresses(employee.getAddresses());
        return employeeRepo.save(emp);
    }

    public void deletetheaddress(int fk_emp_id, int id) {
            employeeRepo.deletetheAddress(fk_emp_id,id);
    }
}
