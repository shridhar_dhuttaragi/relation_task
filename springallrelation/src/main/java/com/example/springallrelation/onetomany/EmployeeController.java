package com.example.springallrelation.onetomany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.el.MethodNotFoundException;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;
    @RequestMapping("/addEmployee")
    public void add(@Valid @RequestBody Employee employee)
    {
            employeeService.save(employee);
    }

    @RequestMapping("/getEmployee")
    public List<Employee> getAll()
    {
        return employeeService.getAll();
    }
    @RequestMapping("/getEmployee/{id}")
    public Employee getEmployee(@PathVariable int id)
    {
        return employeeService.getEmployeeById(id);
    }
    @RequestMapping("/deleteEmployee/{id}")
    public void deleteEmployee(@PathVariable int id)
    {
        employeeService.deleteEmployeeById(id);
    }

    @RequestMapping("/updateEmployee/{id}")
    public void updateEmployee(@RequestBody Employee employee,@PathVariable int id)
    {
        employeeService.updateEmployeeById(id,employee);
    }
    @RequestMapping("/employeeDetails/{id}/deleteAddress/{id}")
    public void deletetheadrees(@PathVariable("id") int e_id, @PathVariable("id") int a_id)
    {
            employeeService.deletetheaddress(e_id,a_id);
    }



    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String,String> validation(MethodArgumentNotValidException ex)
    {
        Map<String ,String > errors=new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error ->
                errors.put(error.getField(), error.getDefaultMessage()));
        return errors;
    }


}
