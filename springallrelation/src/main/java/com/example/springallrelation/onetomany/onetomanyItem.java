package com.example.springallrelation.onetomany;

import javax.persistence.*;

@Entity
@Table
public class onetomanyItem {
    @Id
    @GeneratedValue
    int id;

    @Column
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
