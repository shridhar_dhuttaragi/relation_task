package com.example.springallrelation.onetomany;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name="Employee_Table")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @NotBlank(message = "Name is mandatory")
    @Size(min = 3,max = 10,message = "Name Should be 4 to 10 Character")
    @Column
    String e_name;

    @NotBlank(message = "Designation is mandatory")
    @Column
    String e_designation;

    @OneToMany(targetEntity = Address.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_emp_id",referencedColumnName = "id")
    List<Address> addresses;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getE_name() {
        return e_name;
    }

    public void setE_name(String e_name) {
        this.e_name = e_name;
    }

    public String getE_designation() {
        return e_designation;
    }

    public void setE_designation(String e_designation) {
        this.e_designation = e_designation;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
