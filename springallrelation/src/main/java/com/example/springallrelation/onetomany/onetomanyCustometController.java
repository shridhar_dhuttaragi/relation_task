package com.example.springallrelation.onetomany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class onetomanyCustometController {
    @Autowired
    onetomanyRepo onetomanyRepos;

    @RequestMapping("/onetomany")
    public void add(@RequestBody onetomanyCustomer onetomanyCustomers)
    {
        onetomanyRepos.save(onetomanyCustomers);
    }
    @RequestMapping("/getonetomany")
    public List<onetomanyCustomer> get()
    {
        return (List<onetomanyCustomer>) onetomanyRepos.findAll();
    }

}
