package com.example.springallrelation.onetomany;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class onetomanyCustomer {
    @Id
    @GeneratedValue
    int id;

    @Column
    String name;


    @OneToMany(targetEntity = onetomanyItem.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_itm_id",referencedColumnName = "id")
    List<onetomanyItem> onetomanyItems;

    public List<onetomanyItem> getOnetomanyItems() {
        return onetomanyItems;
    }

    public void setOnetomanyItems(List<onetomanyItem> onetomanyItems) {
        this.onetomanyItems = onetomanyItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
