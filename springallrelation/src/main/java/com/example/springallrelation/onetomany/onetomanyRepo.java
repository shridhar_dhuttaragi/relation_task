package com.example.springallrelation.onetomany;

import org.springframework.data.repository.CrudRepository;

public interface onetomanyRepo extends CrudRepository<onetomanyCustomer ,Integer> {
}
