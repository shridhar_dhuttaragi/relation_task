package com.example.springallrelation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com/example/springallrelation/onetoone","com/example/springallrelation/onetomany","com/example/springallrelation/manytoone","com/example/springallrelation/manytomany"})
public class SpringallrelationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringallrelationApplication.class, args);
	}

}
