package com.example.springallrelation.onetoone;

import javax.persistence.*;

@Entity
@Table
public class Item {

    @Id
    @GeneratedValue
    int id;

    @Column
    String name;

    //it work when bidirection.
    @OneToOne(cascade = CascadeType.ALL,mappedBy = "item")
    Customer customer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
