package com.example.springallrelation.onetoone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CustomerController {

    @Autowired
    CustomerRepo customerRepo;

    @RequestMapping("/onetoone")
    public void add(@Valid  @RequestBody Customer customer)
    {
        customerRepo.save(customer);
    }
    @RequestMapping("/getonetoone")
    public List<Customer> get()
    {
        return (List<Customer>) customerRepo.findAll();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String,String> Validation (MethodArgumentNotValidException ex){
        Map<String ,String > errors=new HashMap<>();

     ex.getBindingResult().getFieldErrors().forEach(error ->
                errors.put(error.getField(), error.getDefaultMessage()));

   return errors;

    }
}
