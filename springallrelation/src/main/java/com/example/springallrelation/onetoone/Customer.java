package com.example.springallrelation.onetoone;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table
public class Customer {

    @Id
    @GeneratedValue
    int id;

    @Column
    @NotBlank(message = "Name is mandatory")
    @Size(min=4,message = "name should at least 4 Char ")
    String name;

    @OneToOne(targetEntity = Item.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_item_id",referencedColumnName = "id")
    Item item;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
